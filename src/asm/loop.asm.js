const test = `// set result to 0
@result 
M=0

// set counter to 10
@10
D=A
@counter
M=D

(LOOP)
  @10
  D=A
  @result
  M=D+M // Increment result

  @counter
  M=M-1
  D=M
  @LOOP
  D;JGT

(END)
  @END
  0;JMP
`;

export default test;
