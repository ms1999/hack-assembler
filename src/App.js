import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import CRT from "./components/CRT";
import MainUI from "./components/MainUI";
import Header from "./components/Header";
import MainDisplay from "./components/MainDisplay";
import SecondaryDisplay from "./components/SecondaryDisplay";

const DisplayContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const outerContainer = {
  display: "block",
  marginLeft: "10px",
  marginRight: "10px"
};

const App = ({
  assemblyCode,
  demoFiles,
  fileDate,
  fileName,
  handleAssembleClicked,
  handleDemoClicked,
  handleDownloadClicked,
  handleLoadDemoClicked,
  handleUpdateFile,
  isAssembleError,
  isAssembleSuccess,
  isDemoActive,
  isFileLoaded,
  machineCode,
  resetState
}) => (
  <CRT>
    <div>
      <Header />
      <div style={outerContainer}>
        <DisplayContainer>
          <MainDisplay
            label="Input"
            value={assemblyCode || "Welcome to the Hack assembler!"}
          />
          <MainDisplay label="Output" value={machineCode || ""} />
        </DisplayContainer>
        <SecondaryDisplay
          isFileLoaded={isFileLoaded}
          fileName={fileName}
          fileDate={fileDate}
          isAssembleError={isAssembleError}
          isAssembleSuccess={isAssembleSuccess}
        />

        <MainUI
          handleUpdateFile={handleUpdateFile}
          handleAssembleClicked={handleAssembleClicked}
          resetState={resetState}
          isAssembled={!!machineCode}
          isFileLoaded={isFileLoaded}
          handleLoadDemo={handleLoadDemoClicked}
          isDemoActive={isDemoActive}
          handleDemoClicked={handleDemoClicked}
          handleDownloadClicked={handleDownloadClicked}
          demoFiles={demoFiles.map(file => file.fileName)}
          isAssembleError={isAssembleError}
        />
      </div>
    </div>
  </CRT>
);

App.propTypes = {
  assemblyCode: PropTypes.string,
  demoFiles: PropTypes.array,
  fileDate: PropTypes.string,
  handleAssembleClicked: PropTypes.func,
  handleDemoClicked: PropTypes.func,
  handleDownloadClicked: PropTypes.func,
  handleLoadDemoClicked: PropTypes.func,
  handleUpdateFile: PropTypes.func,
  isAssembleError: PropTypes.bool,
  isAssembleSuccess: PropTypes.bool,
  isDemoActive: PropTypes.bool,
  isFileLoaded: PropTypes.bool,
  machineCode: PropTypes.string,
  resetState: PropTypes.func
};

export default App;
