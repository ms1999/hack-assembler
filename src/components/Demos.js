import React from "react";
import PropTypes from "prop-types";

import { FlexButton } from "./MainUI";

const Demos = ({ demoFiles, handleLoadDemo, handleDemoClicked }) => (
  <React.Fragment>
    {demoFiles.map((fileName, i) => (
      <FlexButton
        key={i}
        error={fileName === "error.asm" ? true : false}
        onClick={() => {
          handleLoadDemo(fileName);
        }}
      >
        {fileName}
      </FlexButton>
    ))}
    <FlexButton warning onClick={handleDemoClicked}>
      Back
    </FlexButton>
  </React.Fragment>
);

Demos.propTypes = {
  demoFiles: PropTypes.arrayOf(PropTypes.string),
  handleDemoClicked: PropTypes.func,
  handleLoadDemo: PropTypes.func
};

export default Demos;
