import React from "react";
import styled from "styled-components";
import Logo from "../img/logo.svg";

const StyledImg = styled.img`
  max-width: 600px;
  margin-left: auto;
  margin-right: auto;
  display: block;
`;

const Header = () => <StyledImg src={Logo} />;

export default Header;
