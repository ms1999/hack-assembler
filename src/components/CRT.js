import styled from "styled-components";

const CRT = styled.div`
  position: relative;
  overflow: hidden;
  &:before,
  &:after {
    display: block;
    pointer-events: none;
    content: "";
    position: absolute;
  }
  &:before {
    width: 100%;
    height: 2px;
    z-index: 2147483649;
    background: rgba(0, 0, 0, 0);
    opacity: 0.25;
  }
  &:after {
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 2147483648;
    background: linear-gradient(
      to bottom,
      transparent 50%,
      rgba(0, 0, 0, 0.05) 51%
    );
    background-size: 100% 4px;
  }
`;

export default CRT;
