import React from "react";
import PropTypes from "prop-types";
import { TextArea } from "nes-react";
import styled from "styled-components";

const textAreaStyle = {
  height: "50vh",
  background: "lightgray",
  WebkitAppearance: "none"
};

const ContainerStyled = styled.div`
  flex: 1;
  margin-right: 10px;
  min-width: 300px;
`;

const MainDisplay = ({ label, value }) => (
  <ContainerStyled>
    <TextArea label={label} style={textAreaStyle} value={value} readOnly />
  </ContainerStyled>
);

MainDisplay.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string
};

export default MainDisplay;
