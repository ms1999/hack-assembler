import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button } from "nes-react";
import { slideInLeft, slideInRight } from "react-animations";
import styled, { keyframes } from "styled-components";
import Demos from "./Demos";

const SlideInRight = keyframes`${slideInRight}`;
const SlideInLeft = keyframes`${slideInLeft}`;

const ButtonGroup = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  flex: 1;
`;

const ButtonGroupRight = styled(ButtonGroup)`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  animation: 0.5s ${SlideInRight};
  flex: 1;
`;

const ButtonGroupLeft = styled(ButtonGroup)`
  animation: 0.5s ${SlideInLeft};
`;

export const FlexButton = styled(Button)`
  margin: 10px !important;
  flex: 1;
  min-width: 180px;
  @media (max-width: 992px) {
    flex-basis: calc(100% - 12px);
  }
  @media (max-width: 768px) {
    flex-basis: calc(50% - 12px);
  }
`;

const DownloadButton = styled(FlexButton)`
  @media (max-width: 768px) {
    flex-basis: calc(100% - 12px);
  }
`;

class MainUI extends Component {
  handleImportClick = el => {
    document.querySelector("#fileButton").click();
  };

  render() {
    const {
      demoFiles,
      handleAssembleClicked,
      handleDemoClicked,
      handleLoadDemo,
      handleUpdateFile,
      isAssembled,
      isAssembleError,
      isDemoActive,
      isFileLoaded,
      resetState
    } = this.props;

    return (
      <React.Fragment>
        {!isDemoActive && (
          <ButtonGroupLeft>
            <FlexButton
              disabled={!isFileLoaded || isAssembled} //TODO disable if assembled
              primary
              onClick={handleAssembleClicked}
            >
              Assemble
            </FlexButton>
            <FlexButton onClick={this.handleImportClick}>Import</FlexButton>
            <input
              id="fileButton"
              type="file"
              style={{ display: "none" }}
              onChange={handleUpdateFile}
              accept=".asm"
            />
            <FlexButton warning onClick={handleDemoClicked}>
              Demos
            </FlexButton>
            <FlexButton error disabled={!isFileLoaded} onClick={resetState}>
              Reset
            </FlexButton>
            <DownloadButton
              disabled={!isAssembled || isAssembleError}
              success
              onClick={this.props.handleDownloadClicked}
            >
              Download
            </DownloadButton>
          </ButtonGroupLeft>
        )}

        {isDemoActive && (
          <ButtonGroupRight>
            <Demos
              handleLoadDemo={handleLoadDemo}
              demoFiles={demoFiles}
              handleDemoClicked={handleDemoClicked}
            />
          </ButtonGroupRight>
        )}
      </React.Fragment>
    );
  }
}

MainUI.propTypes = {
  demoFiles: PropTypes.arrayOf(PropTypes.string),
  handleAssembleClicked: PropTypes.func,
  handleDemoClicked: PropTypes.func,
  handleLoadDemo: PropTypes.func,
  handleUpdateFile: PropTypes.func,
  isAssembled: PropTypes.bool,
  isAssembleError: PropTypes.bool,
  isDemoActive: PropTypes.bool,
  isFileLoaded: PropTypes.bool,
  resetState: PropTypes.func
};

export default MainUI;
