import React from "react";
import PropTypes from "prop-types";

import { TextInput } from "nes-react";
import styled from "styled-components";

const Container = styled.div`
  margin-right: 10px;
`;

const SecondaryDisplay = ({
  fileDate,
  fileName,
  isAssembleError,
  isAssembleSuccess
}) => (
  <Container>
    <TextInput
      onChange={() => null}
      style={{ background: "lightgray", WebkitAppearance: "none" }}
      success={isAssembleSuccess}
      error={isAssembleError}
      readOnly
      value={
        fileName
          ? `${fileName} ${fileDate && " Last Modified: "} ${fileDate &&
              fileDate.toLocaleDateString()}`
          : "Please import your .asm file or select a demo:"
      }
    />
  </Container>
);

SecondaryDisplay.propTypes = {
  fileDate: PropTypes.string,
  fileName: PropTypes.string,
  isAssembleError: PropTypes.bool,
  isAssembleSuccess: PropTypes.bool
};

export default SecondaryDisplay;
