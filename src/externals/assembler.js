import { compMap, destinationMap, jumpMap } from "./Maps";

/* ---- BOOLEANS ------------------------------- */
let isAInstructionConstant = str => (/@[0-9]*$/gm.test(str) ? true : false);
let isLabelDefinition = str => (/\(.+\)/gim.test(str) ? true : false);
let isVariableOrLabelReference = str =>
  /^@([a-z]+([\w]+)?|\d+[a-z]+)/gim.test(str) ? true : false;

// TODO move to own module
var symbolTable = {
  SCREEN: 16384,
  KBD: 24576,
  SP: 0,
  LCL: 1,
  ARG: 2,
  THIS: 3,
  THAT: 4
};

for (var i = 0; i < 16; i++) {
  //Add RAM[0-16] to symbolTable
  symbolTable[`R${i}`] = i;
}

/* ----MAPS--------------------------------------------------- */

/* ----PARSERS------------------------------------------------ */
function parseAConstant(str) {
  //A instructions:  @value - @21, @foo binary:  0valueInBinary 0000000000010101
  let binaryString = Number(str.split("@")[1]).toString(2);
  return zeroPrefix(binaryString);
}

function parseC(str) {
  // 1 1 1 a c1 c2 c3 c4 c5 c6 d1 d2 d3 j1 j2 j3
  const dest = str.indexOf("=") > -1 ? str.split("=")[0] : "";
  const comp = str.indexOf(";") > -1 ? str.split(";")[0] : str.split("=")[1];
  const jump = str.split(";")[1] || "";

  return "111" + compMap(comp) + destinationMap(dest) + jumpMap(jump);
}

/* HELPERS ---------- */
function zeroPrefix(str) {
  let prefix = "";

  for (let i = 15; i >= str.length; i--) {
    prefix += "0";
  }
  return prefix + str;
}

function routeString(str) {
  if (!str.length) {
    return;
  } else if (isAInstructionConstant(str)) {
    return parseAConstant(str);
  } else if (isVariableOrLabelReference(str)) {
    const variable = str.split("@")[1];
    const decimal = symbolTable[variable];
    return zeroPrefix(decimal.toString(2));
  }
  return parseC(str);
}

function firstPass(labels, symbolTableObj) {
  // populate symbolTable object with label pointers
  // TODO: refactor to not mutate

  labels.forEach(label => {
    let prop = label.instruction.slice(1, label.instruction.length - 1);
    symbolTableObj[prop] = label.pointer;
  });
}

function secondPass(variables, symbolTableObj) {
  // TODO: refactor to not mutate
  let startingAddress = 16;
  for (let i = 0; i < variables.length; i++) {
    const prop = variables[i];
    symbolTableObj[prop] = startingAddress;
    startingAddress++;
  }
}

function preParse(asm) {
  return asm
    .split("\n")
    .map(line => {
      return line.indexOf("//") > -1
        ? line.substr(0, line.indexOf("//")).trim()
        : line.trim();
    })
    .filter(str => str.length);
}

function returnInstructionsArray(preParsed) {
  let instructions = [];
  let instructionNumber = 0;
  for (let i = 0; i < preParsed.length; i++) {
    const instruction = preParsed[i];
    let obj = { instruction };
    if (isLabelDefinition(instruction)) {
      obj.pointer = instructionNumber;
    } else {
      instructionNumber++;
    }
    instructions.push(obj);
  }
  return instructions;
}

function assemble(asm) {
  const preParsed = preParse(asm); // remove comments, whitespace etc
  const instructionsWithSymbols = returnInstructionsArray(preParsed); // returns instructions array that can identify labels
  const labels = instructionsWithSymbols.filter(el => el.pointer);

  firstPass(labels, symbolTable); // mutates symbolTable

  const variables = instructionsWithSymbols
    .map(el => el.instruction)
    .filter(el => isVariableOrLabelReference(el)) // grab elements that are variables or references to lables
    .filter((el, i, arr) => arr.indexOf(el) === i) // remove duplicates
    .map(el => el.split("@")[1]) // remove @ symbol
    .filter(el => (symbolTable.hasOwnProperty(el) ? false : true)); // remove labels that already exist in symbolTable

  secondPass(variables, symbolTable); // mutates symbolTable

  const instructions = instructionsWithSymbols
    .filter(el => !el.pointer)
    .map(el => el.instruction);

  return instructions
    .map(str => routeString(str.trim()))
    .filter(el => el != null)
    .join("\n");
}

export default assemble;
