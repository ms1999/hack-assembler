import React, { Component } from "react";

import App from "./App";
import assemble from "./externals/assembler";
import loop from "./asm/loop.asm.js";
import pong from "./asm/pong.asm.js";
import error from "./asm/error.asm.js";

class AppContainer extends Component {
  constructor() {
    super();
    this.state = {
      isFileLoaded: false,
      assemblyCode: "",
      fileName: "",
      fileDate: "",
      isAssembleError: false,
      isAssembleSuccess: false,
      isDemoMenuActive: false,
      textFile: "",
      machineCode: ""
    };

    this.demoFiles = [
      { fileName: "loop.asm", fileContents: loop },
      { fileName: "pong.asm", fileContents: pong },
      { fileName: "error.asm", fileContents: error }
    ];
  }

  handleAssembleClicked = e => {
    try {
      const machineCode = assemble(this.state.assemblyCode);
      const textFile = this.createTextFile(this.state.machineCode);

      this.setState({ machineCode, textFile, isAssembleSuccess: true });
    } catch (err) {
      this.setState({
        machineCode: "There was an error parsing your file.",
        isAssembleError: true
      });
    }
  };

  handleDemoClicked = () => {
    this.setState({ isDemoActive: !this.state.isDemoActive });
  };

  handleDownloadClicked = () => {
    var element = document.createElement("a");
    element.setAttribute(
      "href",
      "data:text/plain;charset=utf-8," + encodeURI(this.state.machineCode)
    );
    element.setAttribute(
      "download",
      this.state.fileName.split(".")[0] + ".hack" || "assembled.hack"
    );
    element.style.display = "none";
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  };

  createTextFile = machineCode => {
    const data = new Blob([machineCode], { type: "text/plain" });
    return window.URL.createObjectURL(data);
  };

  handleLoadDemoClicked = fileName => {
    const selectedDemo = this.demoFiles.find(
      demo => demo.fileName === fileName
    );
    this.resetState();
    this.loadDemoCode(fileName, selectedDemo);
  };

  loadDemoCode = (fileName, selectedDemo) => {
    this.setState({
      assemblyCode: selectedDemo.fileContents,
      fileName,
      isFileLoaded: true
    });
  };

  handleUpdateFile = e => {
    const file = e.target.files[0];
    const fileName = file.name;
    const fileDate = file.lastModifiedDate;
    let reader = new FileReader();

    reader.onload = e => {
      let contents = e.target.result;
      this.setState({
        assemblyCode: contents,
        fileName,
        fileDate,
        isFileLoaded: true
      });
    };

    reader.readAsText(file);
  };

  resetState = () => {
    this.setState({
      isAssembleError: false,
      isAssembleSuccess: false,
      isFileLoaded: false,
      assemblyCode: "",
      fileName: "",
      fileDate: "",
      machineCode: ""
    });
  };

  render() {
    const {
      assemblyCode,
      fileDate,
      fileName,
      isAssembleError,
      isAssembleSuccess,
      isDemoActive,
      isFileLoaded,
      machineCode
    } = this.state;

    return (
      <App
        assemblyCode={assemblyCode}
        demoFiles={this.demoFiles}
        fileDate={fileDate}
        fileName={fileName}
        handleAssembleClicked={this.handleAssembleClicked}
        handleDemoClicked={this.handleDemoClicked}
        handleDownloadClicked={this.handleDownloadClicked}
        handleLoadDemoClicked={this.handleLoadDemoClicked}
        isDemoActive={isDemoActive}
        isFileLoaded={isFileLoaded}
        isAssembleError={isAssembleError}
        isAssembleSuccess={isAssembleSuccess}
        machineCode={machineCode}
        handleUpdateFile={this.handleUpdateFile}
        resetState={this.resetState}
      />
    );
  }
}

export default AppContainer;
